<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Book;
use Faker\Generator as Faker;
use Faker\Provider\Barcode;

$factory->define(Book::class, function (Faker $faker) {

    $bc = new Barcode($faker); //creating a new barcode instance to generate a random ISBN
    $internetFaker = new \Faker\Provider\Internet($faker);

    $name = $faker->text; //assign to variable to be reused below.

    return [
        //50% chance to either a 13 or 10 digit long ISBN number
        'isbn' => random_int(1,100)>50 ? $bc->unique()->isbn10():$bc->unique()->isbn13(),

        'name' => $name,

        'description' => $faker->paragraph,

        //ex:- "Second Edition", "Third Edition"
        'edition' => ucwords(ordinalNumbersInWords( random_int(1,10) ) . " edition" ),

        //80% of the data will have a url
        'link' => random_int(1,100)>20 ? $internetFaker->url() : null,

        //TODO::will be implemented later
//        'price' => $faker->randomNumber(2),

        //pick a random status with a 10% chance to be disabled
        'status' => random_int(1,100)>90 ? Book::STATUS_OPTION__DISABLED : Book::STATUS_OPTION__ACTIVE,
    ];
});
