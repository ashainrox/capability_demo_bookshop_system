<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Author;
use Faker\Generator as Faker;

$factory->define(Author::class, function (Faker $faker) {

    $name = $faker->firstName;

    return [
        'name' => $name,

        'description' => $faker->paragraph,

        //80% of the data will have a url
        'link' => random_int(1,100)>20 ? "https://www.google.com/search?q=".$name : null,
    ];
});
