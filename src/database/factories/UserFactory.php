<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
        'role' => random_int(1,100)>80 ? User::ROLE_OPTION__ADMIN : User::ROLE_OPTION__CASHIER, //pick a random role with a 20% chance to be a admin
    ];
});

$factory->state(App\Models\User::class, 'admin', [
    'role' => User::ROLE_OPTION__ADMIN ,
]);


$factory->state(App\Models\User::class, 'cashier', [
    'role' => User::ROLE_OPTION__CASHIER ,
]);
