@extends('adminlte::page')

@section('title', $pageTitle)

@section('content_header')
    <h1>{{$pageTitle}}</h1>
@stop

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        {{$models->links()}}
                        <table class="table">
                            <tr>
                                <th>ISBN</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Edition</th>
                                <th>Link</th>
                                <th>Added On</th>
                                <th class="text-center">Status</th>
                            </tr>
                            @foreach($models as $Model)
                                <tr>
                                    <td><a href="{{route('book.edit', $Model->id)}}">{{$Model->isbn}}</td>
                                    <td>{{\Str::limit($Model->name, 30, ' ...')}}</td>
                                    <td>{{$Model->description ? \Str::limit($Model->description, 10, ' ...') : '-'}}</td>
                                    <td>{{\Str::limit($Model->edition, 10, ' ...')}}</td>
                                    <td>
                                        @if($Model->link)
                                            <a onclick="return confirm('Are you sure you want to open this link?')" target="_blank" href="{{$Model->link}}">
                                                {{\Str::limit($Model->link, 10, ' ...')}}
                                            </a>
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td>{{$Model->created_at}}</td>
                                    <td class="text-center">
                                        <span title="{{$Model->status()}}">{!! $Model->status(true) !!}</span>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        {{$models->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
