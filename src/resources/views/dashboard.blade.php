@extends('adminlte::page')

@section('title', $pageTitle)

@section('content_header')
    <h1>{{$pageTitle}}</h1>
@stop

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">

                            <!-- book-count widget -->
                            <div class="col-md-3 col-sm-6 col-12">
                                <a href="{{route('book.index')}}">
                                    <div class="info-box">
                                        <span class="info-box-icon bg-purple"><i class="fas fa-fw fa-book"></i></span>

                                        <div class="info-box-content text-purple">
                                            <span class="info-box-text">Books</span>
                                            <span class="info-box-number">{{$bookCount}}</span>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </a>
                            </div>
                            <!-- /.col -->
                            <!-- /book-count widget -->


                            <!-- author-count widget -->
                            <div class="col-md-3 col-sm-6 col-12">
                                <a href="{{route('author.index')}}">
                                    <div class="info-box">
                                        <span class="info-box-icon bg-danger"><i class="fas fa-fw fa-user"></i></span>

                                        <div class="info-box-content text-danger">
                                            <span class="info-box-text">Author</span>
                                            <span class="info-box-number">{{$authorCount}}</span>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </a>
                            </div>
                            <!-- /.col -->
                            <!-- /author-count widget -->


                            <!-- tag-count widget -->
                            <div class="col-md-3 col-sm-6 col-12">
                                <a href="{{route('tag.index')}}">
                                    <div class="info-box">
                                        <span class="info-box-icon bg-info"><i class="fas fa-fw fa-tag"></i></span>

                                        <div class="info-box-content text-info">
                                            <span class="info-box-text">Tags</span>
                                            <span class="info-box-number">{{$tagCount}}</span>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </a>
                            </div>
                            <!-- /.col -->
                            <!-- /tag-count widget -->


                            <div class="col-md-3 col-sm-6 col-12">
{{--                                <div class="info-box">--}}
{{--                                    <span class="info-box-icon bg-danger"><i class="far fa-star"></i></span>--}}

{{--                                    <div class="info-box-content text-danger">--}}
{{--                                        <span class="info-box-text">Likes</span>--}}
{{--                                        <span class="info-box-number">93,139</span>--}}
{{--                                    </div>--}}
{{--                                    <!-- /.info-box-content -->--}}
{{--                                </div>--}}
{{--                                <!-- /.info-box -->--}}
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
@stop

@section('js')

@stop
