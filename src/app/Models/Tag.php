<?php

namespace App\Models;

use App\Components\SearchEveryWhere;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model
{
    use SoftDeletes, SearchEveryWhere;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];


    /**
     * Columns white listed for the global search
     */
    protected $searchable = [
        'name'
    ];


    /**
     * Get the books with this tag.
     */
    public function books(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Book::class);
    }
}
