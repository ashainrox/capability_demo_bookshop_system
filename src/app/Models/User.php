<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;


/**
 * @property string name
 * @property string email
 * @property string password
 * @property int role
 * @property Carbon updated_at
 * @property Carbon created_at
 */


class User extends Authenticatable
{
    use Notifiable;

    const ROLE_OPTION__CASHIER = 0;
    const ROLE_OPTION__ADMIN = 1;

    const ROLE_OPTIONS = [
        self::ROLE_OPTION__CASHIER => 'Cashier',
        self::ROLE_OPTION__ADMIN => 'Admin'
    ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function isAdmin(): bool{
        return $this->role == self::ROLE_OPTION__ADMIN;
    }

    public function isCashier(): bool{
        return $this->role == self::ROLE_OPTION__CASHIER;
    }

    public function role(): string{
        return self::ROLE_OPTIONS[$this->role];
    }
}
